package com.example.tp5_edt;


import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import net.fortuna.ical4j.data.CalendarBuilder;
import net.fortuna.ical4j.model.Calendar;
import net.fortuna.ical4j.model.Component;
import net.fortuna.ical4j.model.Property;

import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;


/**
 * A simple {@link Fragment} subclass.
 */
public class FirstFragment extends Fragment  {
    DbHelper db ;
    public ArrayList<Map<String, String>> calendarEntries = new ArrayList<>();
    Map<String, String> calendarEntry ;
    //list of courses
    public List<Course> listCourses = new ArrayList<>();

    TextView CcourseStart;
    TextView CcourseEnd;
    TextView description;
    TextView SecondNextCourse;
    TextView hRest;
    TextView minRest;
    TextView jReste;
    //TextView sReste;
    TextView courseHasStarted;
    Button refresh ;
    Spinner spinner;

    public FirstFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        db = new DbHelper(getContext());
        View view = inflater.inflate(R.layout.fragment_first, container, false);

        CcourseStart = (TextView) view.findViewById(R.id.start);
        CcourseEnd = (TextView) view.findViewById(R.id.end);
        description = (TextView) view.findViewById(R.id.next_course);
        SecondNextCourse = (TextView) view.findViewById(R.id.second_next_course);
        hRest = (TextView) view.findViewById(R.id.h_rest);
        minRest = (TextView) view.findViewById(R.id.m_rest);
        jReste = (TextView) view.findViewById(R.id.j_rest);
        //sReste = (TextView) view.findViewById(R.id.s_rest);
        courseHasStarted = (TextView) view.findViewById(R.id.already_start_msg);
        refresh = (Button) view.findViewById(R.id.refresh);



        String[] arraySpinner = new String[] {
                "1", "2", "3", "4", "5", "6", "7"
        };
        /*spinner = (Spinner) view.findViewById(R.id.spinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, arraySpinner);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);/*/


       /* spinner =  view.findViewById(R.id.spinner);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.sppiner_items, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);
          */











        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nextCourseView();

            }
        });


        //executeUpdateThread("https://edt-api.univ-avignon.fr/app.php/api/exportAgenda/tdoption/3390,3391,3392,3393,3394,3395,28400,28399,28401");
        nextCourseView();
        return view;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }



    public void nextCourseView(){
        try {
            listCourses = db.getNextCourse();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat dateFormatHourAndM = new SimpleDateFormat(
                "HH:mm");
        dateFormatHourAndM.setTimeZone(TimeZone.getTimeZone("UTC"));

        Date DTStart = listCourses.get(0).getDTSTART();
        Date DTEnd = listCourses.get(0).getDTEND();

        String hourStart = dateFormatHourAndM.format(DTStart);
        String hourEnd = dateFormatHourAndM.format(DTEnd);

        System.out.print(listCourses.toString());
        CcourseStart.setText(hourStart);
        CcourseEnd.setText(hourEnd);
        description.setText(listCourses.get(0).getDESCRIPTION());
        SecondNextCourse.setText(listCourses.get(1).getSUMMARRY());

        long milliseconds =listCourses.get(0).getDTSTART().getTime() -  java.util.Calendar.getInstance().getTime().getTime() ;
        /*long seconds = diff / 1000;
        long minutes = seconds / 60;
        long hours = minutes / 60;
        long days = hours / 24;
        //Date resyPeriod=new Date(diff);*/

        //convert the millisec to day, hour and sec
        final long dy = TimeUnit.MILLISECONDS.toDays(milliseconds);
        final long hr = TimeUnit.MILLISECONDS.toHours(milliseconds)
                - TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(milliseconds));
        final long min = TimeUnit.MILLISECONDS.toMinutes(milliseconds)
                - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(milliseconds));
        final long sec = TimeUnit.MILLISECONDS.toSeconds(milliseconds)
                - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(milliseconds));

                    hRest.setText("" + (hr -2));
                    minRest.setText("" + min);
                    jReste.setText("" + dy);
                    //sReste.setText("" + sec);
        if(min<=0)
            courseHasStarted.setVisibility(View.VISIBLE);
    }


}
