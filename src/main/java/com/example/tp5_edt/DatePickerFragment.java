package com.example.tp5_edt;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.DatePicker;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

public  class DatePickerFragment extends DialogFragment
        implements DatePickerDialog.OnDateSetListener {
    View view;
    DbHelper db ;
    //list of courses
    public List<Course> listCourses = new ArrayList<>();
    private RecyclerView recyclerView;
    private CourseAdapter mAdapter;
    CalenderReader calenderReader = new CalenderReader();
    public void updateView(DbHelper dbHelper,List<Course> courses,RecyclerView recyclerView1,CourseAdapter courseAdapter){
        db = dbHelper;
        listCourses = courses;
        recyclerView = recyclerView1;
        mAdapter = courseAdapter;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        // Create a new instance of DatePickerDialog and return it
        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {
        // Do something with the date chosen by the user
        String date  = "" + year + "-" + month + "-" + day;
        System.out.print(date);
          Snackbar.make(view, date, Snackbar.LENGTH_LONG)
         .setAction("Action", null).show();


        try {
            listCourses = db.getAllCourses(year+"-"+month+"-"+day);
        } catch (
                ParseException e) {
            e.printStackTrace();
        }
        mAdapter = new CourseAdapter(listCourses);
        mAdapter.notifyDataSetChanged();
        recyclerView.setAdapter(mAdapter);
        System.out.print(listCourses.toString());

    }
}