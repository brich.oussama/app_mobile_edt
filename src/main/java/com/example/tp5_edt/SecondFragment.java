package com.example.tp5_edt;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;

import net.fortuna.ical4j.data.CalendarBuilder;
import net.fortuna.ical4j.model.Calendar;
import net.fortuna.ical4j.model.Component;
import net.fortuna.ical4j.model.Property;
import net.fortuna.ical4j.util.MapTimeZoneCache;

import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public class SecondFragment extends Fragment {

    DbHelper db ;
    java.util.Calendar calendar = java.util.Calendar.getInstance();

    int year ;
    int month;
    int dayOfMonth;

    public ArrayList<Map<String, String>> calendarEntries = new ArrayList<>();
    Map<String, String> calendarEntry ;
    //list of courses
    public List<Course> listCourses = new ArrayList<>();
    private RecyclerView recyclerView;
    private CourseAdapter mAdapter;
    CalenderReader calenderReader = new CalenderReader();
    TextView noCourses;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    protected View mView;


    public SecondFragment() {
        // Required empty public constructor
    }



    // TODO: Rename and change types and number of parameters
    public static SecondFragment newInstance(String param1, String param2) {
        SecondFragment fragment = new SecondFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_second, container, false);
        this.mView = view;
        db = new DbHelper(getContext());
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        noCourses =(TextView) view.findViewById(R.id.no_course);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        year = calendar.get(java.util.Calendar.YEAR);
        month = calendar.get(java.util.Calendar.MONTH);
        dayOfMonth = calendar.get(java.util.Calendar.DAY_OF_MONTH);
        getDayPerDate(year, month, dayOfMonth);


        Button btnSelect = view.findViewById(R.id.date_select);

        btnSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //  Snackbar.make(view, "heloo", Snackbar.LENGTH_LONG)
                // .setAction("Action", null).show();
                /*DialogFragment newFragment = new DatePickerFragment();
                newFragment.show(getFragmentManager(), "datePicker");
                ((DatePickerFragment) newFragment).updateView(db,listCourses, recyclerView,mAdapter);
                //executeUpdateThread("https://edt-api.univ-avignon.fr/app.php/api/exportAgenda/diplome/2-L3IN");
                newFragment.on*/

                java.util.Calendar calendar = java.util.Calendar.getInstance();
                int year = calendar.get(java.util.Calendar.YEAR);
                int month = calendar.get(java.util.Calendar.MONTH);
                int dayOfMonth = calendar.get(java.util.Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                                getDayPerDate(year, month, day);
                            }


                        }, year, month, dayOfMonth);
                datePickerDialog.show();
            }
        });

        return view;

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }


        //executeUpdateThread("https://edt-api.univ-avignon.fr/app.php/api/exportAgenda/tdoption/3390,3391,3392,3393,3394,3395,28400,28399,28401");

    }




    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    private void getDayPerDate(int year, int month, int day) {
        noCourses.setVisibility(View.INVISIBLE);
        String dayString ;
        String monthString;

        //date picker return wrong month
        if(month < 10)
            monthString = "0" + (month+1);
        else
            monthString = ""+(month+1);


        if(day < 10)
            dayString  = "0" + day ;
        else
            dayString = ""+day;


        try {
            listCourses = db.getAllCourses(year+"-"+monthString+"-"+dayString);
        } catch (
                ParseException e) {
            e.printStackTrace();
        }
        if(listCourses.size()==0)
            noCourses.setVisibility(View.VISIBLE);

        mAdapter = new CourseAdapter(listCourses);
        mAdapter.notifyDataSetChanged();
        recyclerView.setAdapter(mAdapter);
        System.out.print(listCourses.toString());
    }

}
