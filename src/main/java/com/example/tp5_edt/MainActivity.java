package com.example.tp5_edt;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ProgressBar;


import net.fortuna.ical4j.data.CalendarBuilder;
import net.fortuna.ical4j.model.Calendar;
import net.fortuna.ical4j.model.Component;
import net.fortuna.ical4j.model.Property;
import net.fortuna.ical4j.util.MapTimeZoneCache;

import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public class MainActivity extends AppCompatActivity
    implements NavigationView.OnNavigationItemSelectedListener,FirstFragment.OnFragmentInteractionListener,
        SecondFragment.OnFragmentInteractionListener,Configuration.OnFragmentInteractionListener,EvaluationFragment.OnFragmentInteractionListener{
    Fragment fragment = null;

    DbHelper db ;
    public ArrayList<Map<String, String>> calendarEntries = new ArrayList<>();
    Map<String, String> calendarEntry ;
    ProgressBar progressBar;



    @Override
    public void onFragmentInteraction(Uri uri){
        //you can leave it empty
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        setTitle("Emploi du Temps");


        db = new DbHelper(this);

        System.setProperty("net.fortuna.ical4j.timezone.cache.impl", MapTimeZoneCache.class.getName());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        navigationView.setNavigationItemSelectedListener(this);

        //set default fragment
        fragment = new FirstFragment();
        FragmentManager fragmentManager =getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.fragment,fragment).commit();

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }





    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            executeUpdateThread("https://edt-api.univ-avignon.fr/app.php/api/exportAgenda/tdoption/3390,3391,3392,3393,3394,3395,28400,28399,28401");
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();


        if (id == R.id.nav_camera) {
            fragment = new FirstFragment();
            FragmentManager fragmentManager =getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.fragment,fragment).commit();
        } else if (id == R.id.nav_gallery) {

            fragment = new SecondFragment();
            FragmentManager fragmentManager =getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.fragment,fragment).commit();

        } else if (id == R.id.nav_manage) {

            fragment = new Configuration();
            FragmentManager fragmentManager =getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.fragment,fragment).commit();

        } else if (id == R.id.next_quez) {

            fragment = new EvaluationFragment();
            FragmentManager fragmentManager =getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.fragment,fragment).commit();

        }

        else if (id == R.id.nav_share) {

        }
        //replacing the fragment

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }





    public void executeUpdateThread(String url){

        MainActivity.Update update = new MainActivity.Update();
        update.execute(url);
        progressBar.setVisibility(View.VISIBLE);

    }

    public class Update extends AsyncTask<String, Void, net.fortuna.ical4j.model.Calendar> {


        protected net.fortuna.ical4j.model.Calendar doInBackground (String...urls){
            net.fortuna.ical4j.model.Calendar calendar = null;
            URLConnection urlConnection = null;

            try {
                URL url = new URL(urls[0]);
                urlConnection = url.openConnection();
                Log.d(" connect :", "Ue");

                // urlConnection.connect();
                InputStream inputStream = urlConnection.getInputStream();
                Log.d("inputStream :", inputStream.toString());
                CalendarBuilder builder = new CalendarBuilder();
                calendar = builder.build(inputStream);

            } catch (Exception e) {
                Log.d("connect :", "er");
                e.printStackTrace();
            } finally {
            }

            return calendar;
        }

        @Override
        protected void onPostExecute (Calendar calendar){
            for (Iterator i = calendar.getComponents().iterator(); i.hasNext(); ) {
                Component component = (Component) i.next();
                if (component.getName().equalsIgnoreCase("VEVENT")) {
                    calendarEntry = new HashMap<>();
                    for (Iterator j = component.getProperties().iterator(); j.hasNext(); ) {
                        net.fortuna.ical4j.model.Property property = (Property) j.next();
                        calendarEntry.put(property.getName(), property.getValue());
                    }
                    calendarEntries.add(calendarEntry);
                }

            }
            db.dropTable();
            try {
                db.addCourses(calendarEntries);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            progressBar.setVisibility(View.INVISIBLE);

            super.onPostExecute(calendar);
        }
    }


}